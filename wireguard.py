from traceback import print_exc
from system import run_command
from os import path, unlink
from uuid import uuid4


def setup_wireguard_tunnel(config):
	try:
		tmp_file_path = path.join('/tmp', uuid4().hex)
		with open(tmp_file_path, 'w+') as tmpfile:
			tmpfile.write(config['wireguard']['local']['private_key'])
		commands = [
			f"ip link add dn42_{config['short_name']} type wireguard",
			f"ip address add dev dn42_{config['short_name']} {config['wireguard']['local']['ip']} peer {config['wireguard']['peer']['ip']}",
			f"wg set dn42_{config['short_name']} listen-port {config['wireguard']['local']['port']} private-key {tmp_file_path} peer {config['wireguard']['peer']['public_key']} allowed-ips 172.16.0.0/12 endpoint {config['wireguard']['peer']['endpoint']}:{config['wireguard']['peer']['port']}",
			f"ip link set dev dn42_{config['short_name']} up"
		]
		for command in commands:
			response = run_command(command)
			if response.get("error"):
				delet_tmp_file(tmp_file_path)
				print(f"Failed to run command {command}\n{response['error']}")
				destroy_wireguard_tunnel(config)
				return False
		delet_tmp_file(tmp_file_path)
		print(f"Setup new wireguard tunnel for {config['name']}")
		return True
	except Exception: #If there's a failure, make sure to cleanup anything that was created
		print(print_exc())
		delet_tmp_file(tmp_file_path)
		destroy_wireguard_tunnel(config)
		return False

def delet_tmp_file(path):
	unlink(path)

def destroy_wireguard_tunnel(config):
	return run_command(f"ip link delete dn42_{config['short_name']}")['exit_code']