import subprocess

def run_command(command, shell=False):
	try:
		process = subprocess.run(command.split(), timeout=5, stdout=subprocess.PIPE, shell=shell)
	except subprocess.CalledProcessError as e:
		print("ERROR: Command failed: command")
		print(print_exc())
		return {
			"exit_code": 1
		}

	response = {
		"exit_code": process.returncode,
		"output": process.stdout.decode()
	}

	if process.returncode: #Return code was greater than 0, thus the command failed
		response["error"] = True
	
	return response