from os import path
from system import run_command

def setup_bgp_peering(config):
    file_path = path.join("/etc/bird/peers", f"{config['name']}.conf")
    with open(file_path, "wb+") as peer_file:
        file_contents = [
            f"protocol bgp {config['name']} from dnpeers {{",
            f"\tneighbor {config['bgp']['peer']['ip']} as {config['bgp']['peer']['asn']};",
            "}"
        ]
        peer_file.write('\n'.join(file_contents).encode())
    if run_command("bird -p")['exit_code']:
        print(f"ERROR: Bird didn't like config file we created at {file_path}")
        return False
    else:
        run_command("systemctl reload bird")
        return True


