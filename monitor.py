import yaml
from time import sleep
from os import walk, path, getuid, unlink
from traceback import print_exc
from wireguard import setup_wireguard_tunnel, destroy_wireguard_tunnel
from router import setup_bgp_peering
from system import run_command

def clean_slate():
	interfaces = run_command("ip link show")['output'].split("\n")
	for interface in interfaces:
		if "dn42" not in interface:
			continue

		if_name = interface.split()[1][:-1]
		
		run_command(f"ip link delete {if_name}")

	configs_dir = "/etc/bird/peers"
	for root, dirs, files  in walk(configs_dir):
		for file_name in files:
			unlink(path.join(root, file_name))



def apply_config(file_path):
	try:
		with open(file_path) as yml:
			config = yaml.safe_load(yml)
			if config.get("enabled") != True:
				print(f"Configuration for {config['name']} is DISABLED")
				return

			if not (config.get("wireguard") or config.get("bgp")):
				print(f"ERROR: Invalid config found in {file_path}")
				print(config)
				return

			# Configure Wireguard tunnel
			if not setup_wireguard_tunnel(config):
				print(f"Failed to setup wireguard tunnel for {config['name']}")
				return False

			if not setup_bgp_peering(config):
				print(f"Failed to setup BGP Peering for {config['name']}")
				destroy_wireguard_tunnel(config)
				return False

			print(f"Successfully setup new peering with {config['name']}")
			return True
	except FileNotFoundError:
		print(f"Couldn't find YAML file: {file_path}")
		return False
	except yaml.scanner.ScannerError as e:
		print(f"ERROR parsing YAML file: {e}")
		return False




def startup():
	configs_dir = "../dataFiles"
	for root, dirs, files  in walk(configs_dir):
		for file_name in files:
			apply_config(path.join(root, file_name))


if __name__ == '__main__':
	if getuid() > 0:
		exit("Error: Monitor must be run as root")

	try:
		clean_slate() # TODO: Maybe try to get running state rather than wiping everything and starting again
		startup()
	except KeyboardInterrupt:
		exit("Monitor shutdown.")